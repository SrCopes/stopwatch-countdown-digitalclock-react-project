import React, { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import "./css/CountDown.css";

export default function Countdown() {
  const [time, setTime] = useState("");
  useEffect(() => {
    let countDownDate = new Date("April 17, 2022 22:45:00").getTime();
    let x = setInterval(() => {
      let now = new Date().getTime();
      let distance = countDownDate - now;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      setTime(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");
      if (distance < 0) {
        clearInterval(x);
        setTime("IT'S OVER MAN!");
      }
    }, 1000);
  }, []);
  return (
    <div className="countdown">
      <Card sx={{ width: 400 }} color="#dfdfdf">
        <CardContent>
          <Typography sx={{ fontSize: 24 }} color="#333">
            Time until birthday: 🎂
          </Typography>
          <Typography sx={{ fontSize: 15 }} color="#a8a8a8">
            {time}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}
