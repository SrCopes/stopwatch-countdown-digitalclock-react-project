import React from "react";
import { NavLink } from "react-router-dom";
import "./css/Navbar.css";

export default function NavBar() {
  return (
    <>
      <header>
        <navbar className="navbar">
          <NavLink to={"/"}>Current Time</NavLink>
          <NavLink to={"/birthday"}>Birthday</NavLink>
          <NavLink to={"/timer"}>Timer</NavLink>
        </navbar>
      </header>
    </>
  );
}
