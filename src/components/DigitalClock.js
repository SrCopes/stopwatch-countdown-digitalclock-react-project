import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import "./css/DigitalClock.css";

export default function DigitalClock() {
  const [clockState, setClockState] = useState();

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
    }, 1000);
  }, []);

  return (
    <div className="digital-clock">
      <Card sx={{ width: 400 }} color="#dfdfdf">
        <CardContent>
          <Typography sx={{ fontSize: 24 }} color="#333">
            Current time:
          </Typography>
          <Typography sx={{ fontSize: 34 }} color="#a8a8a8">
            🕰️ {clockState}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}
