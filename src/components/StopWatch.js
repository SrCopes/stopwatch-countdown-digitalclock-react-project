import React, { useState, useEffect } from "react";
import PlayCircleFilledIcon from "@mui/icons-material/PlayCircleFilled";
import StopCircleIcon from "@mui/icons-material/StopCircle";
import RotateLeftIcon from "@mui/icons-material/RotateLeft";
import PresentToAllRoundedIcon from "@mui/icons-material/PresentToAllRounded";
import "./css/StopWatch.css";

export default function Stopwatch() {
  const [time, setTime] = useState(0);
  const [timerOn, setTimerOn] = useState(false);
  useEffect(() => {
    let interval = null;
    if (timerOn) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 10);
      }, 10);
    } else {
      clearInterval(interval);
    }

    return () => clearInterval(interval);
  }, [timerOn]);
  return (
    <div className="stopwatch">
      <h2>{time} ⏱️</h2>
      <div id="buttons">
        {!timerOn && time === 0 && (
          <button className="btn" onClick={() => setTimerOn(true)}>
            <PlayCircleFilledIcon />
          </button>
        )}
        {timerOn && (
          <button className="btn" onClick={() => setTimerOn(false)}>
            <StopCircleIcon />
          </button>
        )}
        {!timerOn && time > 0 && (
          <button className="btn" onClick={() => setTime(0)}>
            <RotateLeftIcon />
          </button>
        )}
        {!timerOn && time > 0 && (
          <button className="btn" onClick={() => setTimerOn(true)}>
            <PresentToAllRoundedIcon />
          </button>
        )}
      </div>
    </div>
  );
}
