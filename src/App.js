import "./App.css";
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Countdown from "./components/CountDown";
import DigitalClock from "./components/DigitalClock";
import Stopwatch from "./components/StopWatch";
import NavBar from "./components/Navbar";

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <>
          <NavBar />
          <Switch>
            <Route exact path={"/"} component={DigitalClock}></Route>
            <Route exact path={"/birthday"} component={Countdown}></Route>
            <Route exact path={"/timer"} component={Stopwatch}></Route>
          </Switch>
        </>
      </BrowserRouter>
    </div>
  );
}
